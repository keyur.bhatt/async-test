Flask==0.12.2
gunicorn==19.7.1
gevent==1.3
google-cloud
google-cloud-vision
requests