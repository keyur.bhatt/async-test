FROM ubuntu:18.04

RUN rm -rf /var/lib/apt/lists/*
RUN apt-get clean

RUN apt-get update
RUN apt-get install -y zbar-tools libzbar-dev python-zbar
RUN apt-get install -y build-essential libgtk-3-dev libboost-all-dev
#RUN dpkg -L libzbar-dev; ls -l /usr/include/zbar.h
#RUN apt-get update
#RUN apt-get remove -y ghostscript
#RUN apt-get install -y ghostscript-x
RUN apt-get install -y python3.6
#RUN rm /usr/bin/python
#RUN ln -s /usr/bin/python3.6 /usr/bin/python
WORKDIR /app
ADD . /app
RUN apt-get install -y python3-pip
RUN pip3 install --upgrade pip
#RUN pip3 install setuptools --upgrade
RUN pip3 install -r requirements.txt
#RUN apt-get install python3-wand

#Make change to ImageMagick System Files
#RUN sed -i 's/rights="none"/rights="read|write"/g' /etc/ImageMagick-6/policy.xml
#RUN sed -E "s/(.*<policy.*domain.*resource.*name.*memory.*value.*\")(.*)(\"\/>)/\1"8GiB"\3/" /etc/ImageMagick-6/policy.xml > policy1.xml
#RUN sed -E "s/(.*<policy.*domain.*resource.*name.*disk.*value.*\")(.*)(\"\/>)/\1"12GiB"\3/" policy1.xml > policy2.xml
#RUN mv policy2.xml /etc/ImageMagick-6/policy.xml

RUN ls
#CMD exec gunicorn -b :$PORT -c gunicornconfig.py --graceful-timeout 300 -t 600 main:app

CMD exec gunicorn -b :$PORT -k gevent -w 15 --graceful-timeout 300 -t 600 app:app

