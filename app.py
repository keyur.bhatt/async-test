from flask import Flask
from google.cloud import vision
from google.cloud.vision import types
from flask import Response
import requests
import time
app = Flask(__name__)
 
@app.route("/test-async/")
def test_async():
	
	#Basic - hello world test
	# test_string = "Hello World"
	# print (test_string)
	# time.sleep(5)
	# print (test_string)
	# return test_string

	# Basic - requests library test
	# r = requests.get("https://stackoverflow.com")
	# print(r.status_code)
	# return str(r.status_code)

	# Basic - vision api call test
	client = vision.ImageAnnotatorClient()
	image = vision.types.Image()
	image.source.image_uri = 'https://www.safetysign.com/images/source/large-images/H1523.png'
	resp = client.text_detection(image=image)
	text = str('<br>'.join([d.description for d in resp.text_annotations]))
	print(text)
	return Response(str(resp.text_annotations),status=200, mimetype='application/json')


@app.route("/test-request/")
def test_request():
	
	#Basic - hello world test
	# test_string = "Hello World"
	# print (test_string)
	# time.sleep(5)
	# print (test_string)
	# return test_string

	# Basic - requests library test
	r = requests.get("https://stackoverflow.com")
	print(r.status_code)
	return str(r.status_code)

	# Basic - vision api call test
	# client = vision.ImageAnnotatorClient()
	# image = vision.types.Image()
	# image.source.image_uri = 'https://www.safetysign.com/images/source/large-images/H1523.png'
	# resp = client.text_detection(image=image)
	# text = str('<br>'.join([d.description for d in resp.text_annotations]))
	# print(text)
	# return Response(str(resp.text_annotations),status=200, mimetype='application/json')
	


if __name__ == "__main__":

	app.run()